package com.programcreek.helloWorld.Service;

import java.util.List;

import com.programcreek.helloworld.Pojo.EmployeeEntityPOJO;

public interface ServiceInterface {


	public EmployeeEntityPOJO addEmployee(EmployeeEntityPOJO EE);
	public List<EmployeeEntityPOJO> getAllEmp();
	public void deleteEmployee(Integer EmpId);
}
