package com.programcreek.helloWorld.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.programcreek.helloworld.Pojo.EmployeeEntityPOJO;
import com.programcreek.hellworld.DaoClass.DaoClassInterface;

@Service
public class ServiceClass implements ServiceInterface{

	@Autowired
	private DaoClassInterface classDemo;
	
	public EmployeeEntityPOJO addEmployee(EmployeeEntityPOJO EE) {
		
		return classDemo.addEmployee(EE);
		
	}

	public ServiceClass() {
		// TODO Auto-generated constructor stub
	}
	@Transactional
	public List<EmployeeEntityPOJO> getAllEmp() {
		// TODO Auto-generated method stub
		return classDemo.getAllEmp();
	}

	@Transactional
	public void deleteEmployee(Integer EmpId) {
		// TODO Auto-generated method stub
		classDemo.deleteEmployee(EmpId);
	}

	public void setEmpDao(DaoClassInterface classDemo) {
		this.classDemo=classDemo;
	}
}
