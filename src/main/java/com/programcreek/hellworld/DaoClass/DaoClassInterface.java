package com.programcreek.hellworld.DaoClass;

import java.util.List;

import com.programcreek.helloworld.Pojo.EmployeeEntityPOJO;

public interface DaoClassInterface {

	
	public EmployeeEntityPOJO addEmployee(EmployeeEntityPOJO EE);
	public List<EmployeeEntityPOJO> getAllEmp();
	public void deleteEmployee(Integer EmpId);
	
	
	
}
