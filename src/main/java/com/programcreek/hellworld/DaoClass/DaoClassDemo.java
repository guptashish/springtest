package com.programcreek.hellworld.DaoClass;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.programcreek.helloworld.Pojo.EmployeeEntityPOJO;

@Repository
public class DaoClassDemo implements DaoClassInterface {

	@Autowired
	private SessionFactory sessionFactory;

	public DaoClassDemo() {
		// TODO Auto-generated constructor stub
	}
	
	public EmployeeEntityPOJO addEmployee(EmployeeEntityPOJO EE) {
		// TODO Auto-generated method stub
		return (EmployeeEntityPOJO) this.sessionFactory.getCurrentSession().save(EE);
	}

	public List<EmployeeEntityPOJO> getAllEmp() {
		// TODO Auto-generated method stub
		return this.sessionFactory.getCurrentSession().createQuery("from EmployeeEntityPOJO").list();
	}

	public void deleteEmployee(Integer EmpId) {
		// TODO Auto-generated method stub
		EmployeeEntityPOJO employeeEntityPOJO= (EmployeeEntityPOJO)this.sessionFactory.getCurrentSession().load(EmployeeEntityPOJO.class,EmpId);
			if(null != employeeEntityPOJO) {
				this.sessionFactory.getCurrentSession().delete(EmpId);
			}
				
	}

}