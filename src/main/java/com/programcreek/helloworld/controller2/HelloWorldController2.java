package com.programcreek.helloworld.controller2;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloWorldController2 {

	String message = "Welcome to Spring MVC!";

	@RequestMapping(value = "hello3", method = RequestMethod.GET)
	public ModelAndView showMessage(
			@RequestParam(value = "name", required = false, defaultValue = "World") String name) {
		System.out.println("in controller");

		ModelAndView mv = new ModelAndView("helloworld");
		mv.addObject("message", message);
		mv.addObject("name", name);
		return mv;
	}
	
	@RequestMapping(value = "hello4", method = RequestMethod.GET)
	public ModelAndView showMessage2() {
		System.out.println("in controller 2");

		ModelAndView mv = new ModelAndView("helloworld2");
		return mv;
	}
	
	

}
