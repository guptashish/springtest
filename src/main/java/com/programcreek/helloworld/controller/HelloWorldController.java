package com.programcreek.helloworld.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloWorldController {

	String message = "Welcome to Spring MVC!";
	
	/*@RequestMapping(value="/testrest", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public EmployeeEntityPOJO resttest() {
		return new EmployeeEntityPOJO(1, "ABC", "BCD", "abc@bcd.com", "956022511545544656");
	}*/
	
	@RequestMapping(value = "hello", method = RequestMethod.GET)
	public ModelAndView showMessage(
			@RequestParam(value = "name", required = false, defaultValue = "World") String name) {
		System.out.println("in controller");

		ModelAndView mv = new ModelAndView("helloworld");
		mv.addObject("message", message);
		mv.addObject("name", name);
		return mv;
	}
	
	@RequestMapping(value = "hello2", method = RequestMethod.GET)
	public ModelAndView showMessage2() {
		System.out.println("in controller 2");

		ModelAndView mv = new ModelAndView("helloworld2");
		return mv;
	}
	
	@RequestMapping(value = "helloPathVariable/{name}/{age}/fetchData", method = RequestMethod.GET)
	public ModelAndView showMessage3(
			@PathVariable(value = "name") String name,@PathVariable(value="age") String age) {
		System.out.println("in controller");

		ModelAndView mv = new ModelAndView("helloworldPathVariable");
		mv.addObject("N", name);
		mv.addObject("A", age);
		return mv;
}
}
