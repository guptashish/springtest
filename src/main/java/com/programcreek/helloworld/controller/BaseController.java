package com.programcreek.helloworld.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.programcreek.helloWorld.Service.ServiceInterface;
import com.programcreek.helloworld.Pojo.EmployeeEntityPOJO;

@Controller

public class BaseController {


	private ServiceInterface serviceClass1;
	
	@RequestMapping(value="/testrest", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public EmployeeEntityPOJO resttest() {
		return new EmployeeEntityPOJO(1, "ABC", "BCD", "abc@bcd.com", "956022511545544656");
	}
	
	public BaseController() {
		// TODO Auto-generated constructor stub
	}
	
	@RequestMapping(value="/" ,method = RequestMethod.GET)
	public String listEmployee(ModelMap map) {
		map.addAttribute("employee", new EmployeeEntityPOJO());
		map.addAttribute("employeeList",serviceClass1.getAllEmp());
	
		return "editEmployeeList";
		
	}
	
	
	@RequestMapping(value="/add",method = RequestMethod.POST)
	public EmployeeEntityPOJO addEmployee(@ModelAttribute(value="employee") EmployeeEntityPOJO employeeEntityPOJO, 
			BindingResult bindingResult){
		return serviceClass1.addEmployee(employeeEntityPOJO);
		
		/*return "redirect:/";*/
	}
			
	@RequestMapping(value="/delete/{employeeID}")
	public String deleteEmployee(@PathVariable("employeeID") Integer integer) {
		
		serviceClass1.deleteEmployee(integer);
		
		return "redirect:/";
	}
	

}

