package com.programcreek.helloworld.Pojo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "employee")
public class EmployeeEntityPOJO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5613737417702468123L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;

	@Column(name = "firstName")
	private String FName;

	@Column(name = "lastName")
	private String lName;

	@Column(name = "emailId")
	private String EmailId;

	@Column(name = "TelephoeNum")
	private String TelephoneNumber;

	
	
	public EmployeeEntityPOJO() {
	}

	public EmployeeEntityPOJO(Integer id, String fName, String lName, String emailId, String telephoneNumber) {
		super();
		this.id = id;
		FName = fName;
		this.lName = lName;
		EmailId = emailId;
		TelephoneNumber = telephoneNumber;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFName() {
		return FName;
	}

	public void setFName(String fName) {
		FName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getEmailId() {
		return EmailId;
	}

	public void setEmailId(String emailId) {
		EmailId = emailId;
	}

	public String getTelephoneNumber() {
		return TelephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		TelephoneNumber = telephoneNumber;
	}

}