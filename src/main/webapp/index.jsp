<%@ page language="java" contentType="text/html; charset=ISO-8859-1"

pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Spring 4 MVC - HelloWorld Index Page</title>
</head>
<body>

	<center>
		<h6>Hello World</h6>
		<h3>
			<a href="hello?name=Dreksha">Click Here1</a><br/>
			<a href="hello2"> Click Here 2 </a><br/>
			<a href="hello3?name=Eric">Click Here3</a><br/>
			<a href="helloPathVariable/Dreksha/27/fetchData"> Click Here 4 </a>
		</h3>
	</center>
	
</body>
</html>